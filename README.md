## Revolut backend test

Design and implement a RESTful API (including data model and the backing implementation) for
money transfers between accounts.

 Explicit requirements:  
 1. You can use Java or Kotlin.  
 2. Keep it simple and to the point (e.g. no need to implement any authentication).  
 3. Assume the API is invoked by multiple systems and services on behalf of end users.  
 4. You can use frameworks/libraries if you like (except Spring), but don't forget about  
 requirement #2 and keep it simple and avoid heavy frameworks.  
 5. The datastore should run in-memory for the sake of this test.  
 6. The final result should be executable as a standalone program (should not require a
 pre-installed container/server).  
 7. Demonstrate with tests that the API works as expected.  
 Implicit requirements:  
 1. The code produced by you is expected to be of high quality.  
 2. There are no detailed requirements, use common sense.  
    
---

## Technologies

1. Java
2. Spark Java
3. Lombok
4. Jackson databind
5. junit
6. Gradle

## Running locally
1. I's necessary to have lombok plugin installed in your IDE and annotation process enabled.  
    https://projectlombok.org/setup/intellij  
    https://projectlombok.org/setup/eclipse
    
2. Run the main class com.revolut.accounts.api.AccountsApi to get started.
3. Endpoints will be available at http://localhost:4567

## Endpoints


| Method | Endpoint                | Body                          | Description                     |  
| ------ | ----------------------- | ----------------------------- | ------------------------------- |
| POST   | /accounts               | { "balance" : 100.00}         | Create a new account            |
| PUT    | /accounts/:id           | { "id", 1, balance" : 100.00} | Update an account               |
| GET    | /accounts/:id           |                               | Get an account by its id        |
| POST   | /accounts/:id/transfer  | { "accountIdTo" : 1}          | Transfer money between accounts |
  
---
## Scenarios

## Posting new account

#####1. Invalid balance for new account

    Given an invalid or null balance   
    When post accounts    
    Then the application should:
        - Return htto status 400 bad request
        - show an error "Invalid request body".

#####1. Create account successfully
    
    Given a valid balance   
    When post accounts    
    Then the application should return:
        - Return http status 201
        - the new account created

## Updating an account

#####1. Invalid account to be updated

    Given an invalid account id to be updated   
    When put account   
    Then the application should:
        - Return http status 404 not found
        - show an error Account not found.

#####2. Update account successfully
    
    Given a valid account id and balance   
    When put accounts    
    Then the application should return:
        - Return http status 200
        - the account should be updated
        
## Transfer money between accounts

#####1. Invalid account to be debited

    Given an invalid account id to be debited   
    When transfer   
    Then the application should:
        - Return http status 404 not found
        - show an error Account not found.
    
#####2. Invalid account to be credited
    
    Given an invalid account id to be credited   
    When transfer   
    Then the application should:
        - Return http status 404 not found
        - show an error Account not found.

#####3. No credit available for transfer
    
    Given an account with no available money provided to transfer   
    When transfer   
    Then the application should:
        - Return http status 400 bad request
        - Show an error No credit available for transfer.
        

#####4. Transfer money successfully
    
    Given two valid accounts and the ammount available for transfer   
    When transfer   
    Then the application should return:
        - return http status 201 created
        - A json containing the transfer data
        - add money to the account provided
        - subtract money from the account provides
        
## Getting an account        

#####1. Get a accounts providing non number id 

    Given a non number account id  
    When getting account   
    Then the application should:
        - Return http status 400 bad request
        - show an error For input string ":id".
        
#####2. Get invalid account id

    Given an invalid account id  
    When getting account   
    Then the application should:
        - Return http status 400 bad request
        - show an error Account not found.

#####2. Get a valid account id

    Given a valid account id  
    When getting account   
    Then the application should:
        - Return http status 200 ok
        - Return a json containing the account data.      
    

---

## Simulating deadlocks
In order to simulate a deadlock just change the method transfer in the Account.java class and run the tests.  
Then the the test givenTwoEqualsAccountsInSameOrder_whenTransfer_thenShouldNotDeadLock will fail
```
    public void transfer(Account to, BigDecimal amount) {
        synchronized (to) {
            try {Thread.sleep(10);} catch (InterruptedException e) {}
            System.out.println("Lock account:" + to.getId());
            synchronized (this) {
                System.out.println("Lock account:" + this.getId());

                checkSameAccount(to);
                checkTransferAvailability(amount);
                subtract(amount);
                add(to, amount);
            }
        }
    }
```