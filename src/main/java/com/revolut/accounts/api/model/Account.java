package com.revolut.accounts.api.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.revolut.accounts.api.exception.InvalidAccountTransferException;
import com.revolut.accounts.api.exception.NoCreditAvailableException;
import com.revolut.accounts.api.service.RandomSingleton;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static com.revolut.accounts.api.AccountsConstants.DEBIT_AND_CREDIT_ARE_THE_SAME_MSG;
import static com.revolut.accounts.api.AccountsConstants.TRANSFER_AMOUNT_NOT_AVAILABLE_MSG;

@EqualsAndHashCode
public final class Account {

    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private BigDecimal balance;

    @JsonIgnore
    private final Random random = RandomSingleton.getInstance();

    @JsonIgnore
    private static final Lock lock = new ReentrantLock();

    @JsonCreator
    public Account(@JsonProperty("balance") String balance)
            throws NumberFormatException {
        this.balance = new BigDecimal(balance);
    }

    public void transfer(Account to, BigDecimal amount) {
        lock.lock();
        try {
            checkSameAccount(to);
            checkTransferAvailability(amount);
            subtract(amount);
            add(to, amount);
        } finally {
            lock.unlock();
        }
    }

    private void checkSameAccount(Account to) {
        if (to.getId().equals(this.id))
            throw new InvalidAccountTransferException(DEBIT_AND_CREDIT_ARE_THE_SAME_MSG);
    }

    private void checkTransferAvailability(BigDecimal amount) {
        if (this.balance.subtract(amount).compareTo(BigDecimal.ZERO) < 0)
            throw new NoCreditAvailableException(TRANSFER_AMOUNT_NOT_AVAILABLE_MSG);
    }

    private void subtract(BigDecimal amount) {
        this.balance = this.balance.subtract(amount);
    }

    private void add(Account to, BigDecimal amount) {
        to.setBalance(to.getBalance().add(amount));
    }
}
