package com.revolut.accounts.api.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.revolut.accounts.api.service.RandomSingleton;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.Random;

@Getter
@EqualsAndHashCode
public final class Transaction {
    private final Long id;
    private final Long accountIdTo;
    private final BigDecimal amount;

    @JsonIgnore
    private final Random random = RandomSingleton.getInstance();

    @JsonCreator
    public Transaction(@JsonProperty("accountIdTo") Long accountIdTo,
                       @JsonProperty("amount") BigDecimal amount) {

        this.id = random.nextLong();
        this.accountIdTo = accountIdTo;
        this.amount = amount;
    }
}
