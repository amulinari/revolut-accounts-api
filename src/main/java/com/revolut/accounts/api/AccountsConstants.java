package com.revolut.accounts.api;

public class AccountsConstants {

    public static final String TRANSFER_AMOUNT_NOT_AVAILABLE_MSG = "Account does not have the amount available for the transaction.";
    public static final String ACCOUNT_FROM_NOT_FOUND_MSG = "Account to be debited not found.";
    public static final String ACCOUNT_TO_NOT_FOUND_MSG = "Account to be credited not found.";
    public static final String ACCOUNT_NOT_FOUND_MSG = "Account not found";
    public static final String DEBIT_AND_CREDIT_ARE_THE_SAME_MSG = "Debit account is the same credit account";

    static final String INVALID_BODY_REQUEST = "Invalid request body.";
    static final String ACCOUNTS = "/accounts";
    static final String TRANSFER = "/accounts/:id/transfer";
    static final String ACCOUNTS_ID = "/accounts/:id";
    static final String PARAM_ID = ":id";
}
