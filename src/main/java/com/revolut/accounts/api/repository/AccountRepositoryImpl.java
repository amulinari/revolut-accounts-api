package com.revolut.accounts.api.repository;

import com.revolut.accounts.api.model.Account;

import java.util.List;
import java.util.Map;

public class AccountRepositoryImpl implements AccountRepository {

    private Map<Long, Account> accountsTable = AccountSessionSingleton.getInstance();

    @Override
    public Account saveAccount(Account account) {
        accountsTable.put(account.getId(), account);
        return account;
    }

    @Override
    public Account getAccount(Long id) {
        return accountsTable.get(id);
    }

    @Override
    public List<Account> saveAll(List<Account> accounts) {
        accounts.forEach(account -> accountsTable.put(account.getId(), account));
        return accounts;
    }
}
