package com.revolut.accounts.api.repository;

import com.revolut.accounts.api.model.Transaction;

import java.util.Map;

public class TransactionRepositoryImpl implements TransactionRepository {

    private Map<Long, Transaction> sessionFactory = TransactionSessionSingleton.getInstance();

    @Override
    public Transaction saveTransaction(Transaction transaction) {
        sessionFactory.put(transaction.getId(), transaction);
        return transaction;
    }
}
