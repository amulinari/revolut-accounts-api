package com.revolut.accounts.api.repository;

import com.revolut.accounts.api.model.Account;

import java.util.List;

/** Account Repository
 * Responsibility to persist and manage accounts into a database
 *
 */
public interface AccountRepository {
    /**
     * Persist an account into a database
     * @param account user account
     * @return user account
     */
    Account saveAccount(Account account);

    /**
     * Get an account from database
     *
     * @param id of an account
     * @return user account
     */
    Account getAccount(Long id);

    /**
     * Persist a list of account into a database
     *
     * @param accounts list of user account
     * @return a list of user accounts
     */
    List<Account> saveAll(List<Account> accounts);
}
