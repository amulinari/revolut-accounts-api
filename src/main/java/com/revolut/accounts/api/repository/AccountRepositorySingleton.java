package com.revolut.accounts.api.repository;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class AccountRepositorySingleton {

    private volatile static AccountRepository repository;
    private static final Lock lock = new ReentrantLock();

    public static AccountRepository getInstance()
    {
        if (repository == null) {
            lock.lock();
            try {
                if (repository == null)
                    repository = new AccountRepositoryImpl();
            } finally {
                lock.unlock();
            }
        }
        return repository;
    }
}
