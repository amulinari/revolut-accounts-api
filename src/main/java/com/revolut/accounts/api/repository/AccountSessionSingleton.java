package com.revolut.accounts.api.repository;

import com.revolut.accounts.api.model.Account;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class AccountSessionSingleton {

    private volatile static Map<Long, Account> accountsTable;
    private static final Lock lock = new ReentrantLock();

    public static HashMap<Long, Account> getInstance() {
        if (accountsTable == null) {
            lock.lock();
            try {
                if (accountsTable == null)
                    accountsTable = new HashMap<>();
            } finally {
                lock.unlock();
            }
        }
        return (HashMap<Long, Account>) accountsTable;
    }
}
