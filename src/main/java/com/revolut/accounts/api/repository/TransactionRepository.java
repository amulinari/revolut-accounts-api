package com.revolut.accounts.api.repository;

import com.revolut.accounts.api.model.Transaction;

/**
 * Used to access the object Transaction into the database
 */
public interface TransactionRepository {
    /**
     * Persists a Transaction object into the database
     *
     * @param transaction object
     * @return Transaction object
     */
    Transaction saveTransaction(Transaction transaction);
}
