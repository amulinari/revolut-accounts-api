package com.revolut.accounts.api.repository;

import com.revolut.accounts.api.model.Transaction;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class TransactionSessionSingleton {
    private static volatile Map<Long, Transaction> transactionMap;
    private static final Lock lock = new ReentrantLock();

    public static Map<Long, Transaction> getInstance() {
        if (transactionMap == null) {
            lock.lock();
            try {
                if (transactionMap == null) {
                    transactionMap = new HashMap<>();
                }
            } finally {
                lock.unlock();
            }
        }
        return transactionMap;
    }
}
