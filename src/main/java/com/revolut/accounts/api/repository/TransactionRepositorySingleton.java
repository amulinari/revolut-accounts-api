package com.revolut.accounts.api.repository;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class TransactionRepositorySingleton {
    private static volatile TransactionRepository transactionRepository;
    private static final Lock lock = new ReentrantLock();

    public static TransactionRepository getInstance() {
        if (transactionRepository == null) {
            lock.lock();
            try {
                if (transactionRepository == null)
                    transactionRepository = new TransactionRepositoryImpl();
            } finally {
                lock.unlock();
            }
        }
        return transactionRepository;
    }
}
