package com.revolut.accounts.api;


import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.accounts.api.exception.AccountNotFoundException;
import com.revolut.accounts.api.exception.InvalidAccountTransferException;
import com.revolut.accounts.api.exception.NoCreditAvailableException;
import com.revolut.accounts.api.model.Account;
import com.revolut.accounts.api.model.Transaction;
import com.revolut.accounts.api.service.AccountService;
import com.revolut.accounts.api.service.AccountServiceSingleton;
import com.revolut.accounts.api.service.ObjectMapperSingleton;
import spark.Response;

import static com.revolut.accounts.api.AccountsConstants.*;
import static org.eclipse.jetty.http.HttpStatus.BAD_REQUEST_400;
import static org.eclipse.jetty.http.HttpStatus.CREATED_201;
import static spark.Spark.*;

public class AccountsApi {
    private static final AccountService service = AccountServiceSingleton.getInstance();
    private static final ObjectMapper mapper = ObjectMapperSingleton.getInstance();

    public static void main(String[] args) {

        post(ACCOUNTS, (request, response) -> {
            response.status(CREATED_201);
            return mapper.writeValueAsString(
                    service.saveAccount(
                            mapper.readValue(request.body(), Account.class)));
        });

        put(ACCOUNTS_ID, (request, response) ->
                        service.updateAccount(Long.valueOf(request.params(PARAM_ID)),
                                mapper.readValue(request.body(), Account.class)),
                mapper::writeValueAsString);

        post(TRANSFER, (request, response) -> {
            response.status(CREATED_201);
            return mapper.writeValueAsString(
                    service.transfer(Long.valueOf(request.params(PARAM_ID)),
                            mapper.readValue(request.body(), Transaction.class)));
        });

        get(ACCOUNTS_ID, (request, response) ->
                        service.getAccount(Long.valueOf(request.params(PARAM_ID))),
                mapper::writeValueAsString);


        exception(AccountNotFoundException.class, (exception, request, response) ->
                notFound(exception.getMessage()));

        exception(NumberFormatException.class, (exception, request, response) ->
                badRequest(response, exception.getMessage()));

        exception(NoCreditAvailableException.class, (exception, request, response) ->
                badRequest(response, exception.getMessage()));

        exception(InvalidAccountTransferException.class, (exception, request, response) ->
                badRequest(response, exception.getMessage()));

        exception(JsonMappingException.class, (exception, request, response) ->
                badRequest(response, INVALID_BODY_REQUEST));

    }

    private static void badRequest(Response response, String message) {
        response.status(BAD_REQUEST_400);
        response.body(message);
    }
}