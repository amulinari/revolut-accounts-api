package com.revolut.accounts.api.exception;

public class NoCreditAvailableException extends RuntimeException {
    public NoCreditAvailableException(String message) {
        super(message);
    }
}
