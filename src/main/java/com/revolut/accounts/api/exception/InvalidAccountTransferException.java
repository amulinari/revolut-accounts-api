package com.revolut.accounts.api.exception;

public class InvalidAccountTransferException extends RuntimeException {
    public InvalidAccountTransferException(String message) {
        super(message);
    }
}
