package com.revolut.accounts.api.service;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class AccountServiceSingleton {

    private volatile static AccountService service;
    private static final Lock lock = new ReentrantLock();

    public static AccountService getInstance() {
        if (service == null) {
            lock.lock();
            try {
                if (service == null)
                    service = new AccountServiceImpl();
            } finally {
                lock.unlock();
            }
        }
        return service;
    }
}
