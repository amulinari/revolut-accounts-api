package com.revolut.accounts.api.service;

import com.revolut.accounts.api.exception.AccountNotFoundException;
import com.revolut.accounts.api.model.Account;
import com.revolut.accounts.api.model.Transaction;
import com.revolut.accounts.api.repository.AccountRepository;
import com.revolut.accounts.api.repository.AccountRepositorySingleton;

import java.util.Arrays;
import java.util.Random;

import static com.revolut.accounts.api.AccountsConstants.*;
import static java.util.Optional.ofNullable;

public class AccountServiceImpl implements AccountService {

    private AccountRepository accountRepository = AccountRepositorySingleton.getInstance();
    private TransactionService transactionService = TransactionServiceSingleton.getInstance();
    private Random random = RandomSingleton.getInstance();

    @Override
    public Account saveAccount(Account account) {
        account.setId(random.nextLong());
        return accountRepository.saveAccount(account);
    }

    @Override
    public Account updateAccount(Long id, Account account) {
        Account persistedAccount = getAccount(id, ACCOUNT_NOT_FOUND_MSG);
        persistedAccount.setBalance(account.getBalance());
        return accountRepository.saveAccount(persistedAccount);
    }

    @Override
    public Transaction transfer(Long fromId, Transaction transaction) throws AccountNotFoundException {
        Account from = getAccount(fromId, ACCOUNT_FROM_NOT_FOUND_MSG);
        Account to = getAccount(transaction.getAccountIdTo(), ACCOUNT_TO_NOT_FOUND_MSG);

        from.transfer(to, transaction.getAmount());

        accountRepository.saveAll(Arrays.asList(from, to));
        return transactionService.saveTransaction(transaction);
    }

    @Override
    public Account getAccount(Long id) {
        return getAccount(id, ACCOUNT_NOT_FOUND_MSG);
    }

    private Account getAccount(Long id, String message) {
        return ofNullable(accountRepository.getAccount(id))
                .orElseThrow(() -> new AccountNotFoundException(message));
    }
}
