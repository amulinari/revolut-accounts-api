package com.revolut.accounts.api.service;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class TransactionServiceSingleton {
    private volatile static TransactionService transactionService;
    private static final Lock lock = new ReentrantLock();

    public static TransactionService getInstance() {
        if (transactionService == null) {
            lock.lock();
            try {
                if (transactionService == null) {
                    transactionService = new TransactionServiceImpl();
                }
            } finally {
                lock.unlock();
            }
        }
        return transactionService;
    }
}
