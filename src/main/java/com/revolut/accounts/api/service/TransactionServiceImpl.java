package com.revolut.accounts.api.service;

import com.revolut.accounts.api.model.Transaction;
import com.revolut.accounts.api.repository.TransactionRepository;
import com.revolut.accounts.api.repository.TransactionRepositorySingleton;

public class TransactionServiceImpl implements TransactionService {

    private TransactionRepository transactionRepository = TransactionRepositorySingleton.getInstance();

    @Override
    public Transaction saveTransaction(Transaction transaction) {
        return transactionRepository.saveTransaction(transaction);
    }
}
