package com.revolut.accounts.api.service;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ObjectMapperSingleton {

    private volatile static ObjectMapper mapper;
    private static final Lock lock = new ReentrantLock();

    public static ObjectMapper getInstance() {
        if (mapper == null) {
            lock.lock();
            try {
                if (mapper == null)
                    mapper = new ObjectMapper();
            } finally {
                lock.unlock();
            }
        }
        return mapper;
    }
}
