package com.revolut.accounts.api.service;

import com.revolut.accounts.api.model.Transaction;

/**
 * Is used to manage a transaction
 */
public interface TransactionService {

    /**
     * Saves a new transaction
     *
     * @param transaction account transaction
     * @return account transaction
     */
    Transaction saveTransaction(Transaction transaction);
}
