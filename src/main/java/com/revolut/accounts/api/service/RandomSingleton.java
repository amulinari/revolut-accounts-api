package com.revolut.accounts.api.service;

import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class RandomSingleton {

    private volatile static Random random;
    private static final Lock lock = new ReentrantLock();

    public static Random getInstance() {
        if (random == null) {
            lock.lock();
            try {
                if (random == null)
                    random = new Random();
            } finally {
                lock.unlock();
            }
        }
        return random;
    }
}
