package com.revolut.accounts.api.service;

import com.revolut.accounts.api.exception.AccountNotFoundException;
import com.revolut.accounts.api.model.Account;
import com.revolut.accounts.api.model.Transaction;


/** Interface of account services
 * Manages the Accounts domain
 *
 */

public interface AccountService {
    /** Create vaidate and save an account
     *
     * @param account user account
     * @return persisted account
     */
    Account saveAccount(Account account);

    /**
     * Upodate Account
     *
     * @param id      account id
     * @param account user account
     * @return persisted account
     */
    Account updateAccount(Long id, Account account);

    /**
     * Transfer amount from one account to another
     *
     * @param fromAcctId Account Id that will be debited
     * @param transaction transction containing two accounts and an amount
     * @return List of accounts updated
     * @throws AccountNotFoundException exception in case account not found
     */
    Transaction transfer(Long fromAcctId, Transaction transaction) throws AccountNotFoundException;

    /**
     * Returns a persisted account from the id sent
     *
     * @param id users account id
     * @return user account
     */
    Account getAccount(Long id);
}
