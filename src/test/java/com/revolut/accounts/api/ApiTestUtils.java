package com.revolut.accounts.api;

import org.eclipse.jetty.http.HttpStatus;
import spark.utils.IOUtils;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

class ApiTestUtils {

    static TestResponse get(String path) throws IOException {
        HttpURLConnection connection = getConnection("GET", path);
        connection.connect();
        return getTestResponse(connection);
    }

    static TestResponse post(String path, String body) throws IOException {
        HttpURLConnection connection = getConnection("POST", path);

        connection.setDoOutput(true);
        try (OutputStream os = connection.getOutputStream()) {
            byte[] input = body.getBytes(StandardCharsets.UTF_8);
            os.write(input, 0, input.length);
        }

        return getTestResponse(connection);
    }

    static TestResponse put(String path, String body) throws IOException {
        HttpURLConnection connection = getConnection("PUT", path);

        connection.setDoOutput(true);
        try (OutputStream os = connection.getOutputStream()) {
            byte[] input = body.getBytes(StandardCharsets.UTF_8);
            os.write(input, 0, input.length);
        }

        return getTestResponse(connection);
    }

    private static TestResponse getTestResponse(HttpURLConnection connection) throws IOException {
        return new TestResponse(connection.getResponseCode(),
                connection.getResponseCode() == HttpStatus.OK_200 ||
                        connection.getResponseCode() == HttpStatus.CREATED_201
                        ? IOUtils.toString(connection.getInputStream()) : null);
    }

    private static HttpURLConnection getConnection(String method, String path) throws IOException {

        URL url = new URL("http://localhost:4567" + path);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestProperty("Content-Type", "application/json; utf-8");
        connection.setRequestMethod(method);
        return connection;
    }

    static class TestResponse {
        private final String body;
        private final int status;

        TestResponse(int status, String body) {
            this.status = status;
            this.body = body;
        }

        String getBody() {
            return body;
        }

        int getStatus() {
            return status;
        }
    }
}
