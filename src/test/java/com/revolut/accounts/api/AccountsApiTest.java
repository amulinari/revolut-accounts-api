package com.revolut.accounts.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.accounts.api.model.Account;
import com.revolut.accounts.api.model.Transaction;
import com.revolut.accounts.api.service.AccountService;
import com.revolut.accounts.api.service.AccountServiceSingleton;
import com.revolut.accounts.api.service.ObjectMapperSingleton;
import com.revolut.accounts.api.service.RandomSingleton;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.IOException;
import java.util.Random;

import static org.eclipse.jetty.http.HttpStatus.NOT_FOUND_404;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static spark.Spark.awaitInitialization;

@RunWith(JUnit4.class)
public class AccountsApiTest {

    private static final String ACCOUNTS = "/accounts";
    private static final String TRANSFER = "/transfer";
    private AccountService accountService = AccountServiceSingleton.getInstance();
    private Random random = RandomSingleton.getInstance();
    private ObjectMapper mapper = ObjectMapperSingleton.getInstance();

    @Before
    public void setUp() {
        String[] args = {};
        AccountsApi.main(args);
        awaitInitialization();
    }

    @Test
    public void givenAnInvalidId_whenGETAccount_thenReturn404() throws IOException {
        assertEquals(ApiTestUtils.get(ACCOUNTS + "/864").getStatus(), NOT_FOUND_404);
    }

    @Test
    public void givenAString_whenGETAccount_thenReturn400() throws IOException {
        assertEquals(ApiTestUtils.get(ACCOUNTS + "/a").getStatus(), HttpStatus.BAD_REQUEST_400);
    }

    @Test
    public void givenAValidId_whenGETAccount_thenReturn200() throws IOException {
        Account account = accountService.saveAccount(new Account("20.00"));
        assertEquals(ApiTestUtils.get(ACCOUNTS + "/" + account.getId()).getStatus(), HttpStatus.OK_200);
    }

    @Test
    public void givenAValidId_whenGETAccount_thenReturnBody() throws IOException {
        Account account = accountService.saveAccount(new Account("20.00"));
        Account result = mapper.readValue(ApiTestUtils.get(ACCOUNTS + "/" + account.getId()).getBody(), Account.class);

        assertEquals("Account id is different from the one requested", account.getId(), result.getId());
        assertEquals("Account balance is different from the one requested", account.getBalance(), result.getBalance());
    }

    @Test
    public void givenAnInvalidAccountId_whenPOSTTransfer_thenReturn404() throws IOException {
        assertEquals(ApiTestUtils.post(ACCOUNTS + "/0" + TRANSFER, getTxnBody(0L, "10.00")).getStatus(), NOT_FOUND_404);
    }

    @Test
    public void givenAnStringAccountId_whenPOSTTransfer_thenReturn400() throws IOException {
        assertEquals(ApiTestUtils.post(ACCOUNTS + "/a" + TRANSFER, getTxnBody(0L, "10.00")).getStatus(), HttpStatus.BAD_REQUEST_400);
    }

    @Test
    public void givenAccountFromAndToTheSame_whenPOSTTransfer_thenReturn400() throws IOException {
        Account account = accountService.saveAccount(new Account("20.00"));
        assertEquals(ApiTestUtils.post(ACCOUNTS + "/" + account.getId() + TRANSFER, getTxnBody(account.getId(), "10.00")).getStatus(), HttpStatus.BAD_REQUEST_400);
    }

    @Test
    public void givenNoCreditAvailable_whenPOSTTransfer_thenReturn400() throws IOException {
        Account accountFrom = accountService.saveAccount(new Account("0.00"));
        Account accountTo = accountService.saveAccount(new Account("0.00"));
        assertEquals(ApiTestUtils.post(ACCOUNTS + "/" + accountFrom.getId() + TRANSFER, getTxnBody(accountTo.getId(), "10.00")).getStatus(), HttpStatus.BAD_REQUEST_400);
    }

    @Test
    public void givenEmptyJsonObject_whenPOSTTransfer_thenReturn400() throws IOException {
        assertEquals(ApiTestUtils.post(ACCOUNTS + "/" + random.nextFloat() + TRANSFER, "").getStatus(), HttpStatus.BAD_REQUEST_400);
    }

    @Test
    public void givenInvalidJsonObject_whenPOSTTransfer_thenReturn404() throws IOException {
        assertEquals(ApiTestUtils.post(ACCOUNTS + "/" + random.nextLong() + TRANSFER, "{}").getStatus(), NOT_FOUND_404);
    }

    @Test
    public void givenValidAccounts_when_POSTTransfer_thenReturn201_andBody() throws IOException {
        Account accountFrom = accountService.saveAccount(new Account("100.00"));
        Account accountTo = accountService.saveAccount(new Account("0.00"));
        String amount = "0.5";
        ApiTestUtils.TestResponse response =
                ApiTestUtils.post(ACCOUNTS + "/" + accountFrom.getId() + TRANSFER, getTxnBody(accountTo.getId(), amount));

        Transaction result = mapper.readValue(response.getBody(), Transaction.class);

        assertEquals("The status is not 200 OK", HttpStatus.CREATED_201, response.getStatus());
        assertEquals("The id is different from the requested", accountTo.getId(), result.getAccountIdTo());
        assertEquals("The amount is different from the requested", amount, result.getAmount().toString());
    }

    @Test
    public void givenAvalidAccount_when_POSTAccount_thenReturn201_andBody() throws IOException {
        String balance = "150.00";
        ApiTestUtils.TestResponse response = ApiTestUtils.post(ACCOUNTS, "{\"balance\":" + balance + "}");
        Account result = mapper.readValue(response.getBody(), Account.class);

        assertEquals("The status is not 201 Created", HttpStatus.CREATED_201, response.getStatus());
        assertNotNull("The returned id is null", result.getId());
        assertEquals("The balance is different from the requested", balance, result.getBalance().toString());
    }

    @Test
    public void givenAnInvalidId_whenPUTAccount_thenReturn404() throws IOException {
        assertEquals("The status was is not 404", ApiTestUtils.get(ACCOUNTS + "/" + random.nextLong()).getStatus(), NOT_FOUND_404);
    }

    @Test
    public void givenAValidAccountId_when_PUTAccount_thenReturn200_andBody() throws IOException {
        String balance = "150.00";
        String updatedBalance = "150.00";
        Account account = accountService.saveAccount(new Account(balance));
        ApiTestUtils.TestResponse response = ApiTestUtils.put(ACCOUNTS + "/" + account.getId(), "{\"balance\": " + updatedBalance + "}");
        Account result = mapper.readValue(response.getBody(), Account.class);

        assertEquals("The status is not 201 Created", HttpStatus.OK_200, response.getStatus());
        assertEquals("The balance is different from the requested", updatedBalance, result.getBalance().toString());
    }

    private String getTxnBody(Long accountIdTo, String amount) {
        return "{\"accountIdTo\":" + accountIdTo + ",\"amount\":" + amount + "}";
    }
}