package com.revolut.accounts.api.service;

import com.revolut.accounts.api.exception.AccountNotFoundException;
import com.revolut.accounts.api.exception.InvalidAccountTransferException;
import com.revolut.accounts.api.exception.NoCreditAvailableException;
import com.revolut.accounts.api.model.Account;
import com.revolut.accounts.api.model.Transaction;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class AccountServiceImplTest {

    private AccountService accountService = AccountServiceSingleton.getInstance();
    private Random random = RandomSingleton.getInstance();

    @Test(expected = AccountNotFoundException.class)
    public void givenAnInvalidAccountFromId_whenTransfer_thenThrowAccountNotFoundException() throws AccountNotFoundException {
        //Given
        Account account = accountService.saveAccount(new Account("0.50"));
        Transaction transaction = new Transaction(account.getId(), new BigDecimal(100.00));

        //When
        accountService.transfer(random.nextLong(), transaction);
    }

    @Test(expected = AccountNotFoundException.class)
    public void givenAnInvalidAccountToId_whenTransfer_thenThrowAccountNotFoundException() throws AccountNotFoundException {
        //Given
        Account account = accountService.saveAccount(new Account("0.50"));
        Transaction transaction = new Transaction(account.getId(), new BigDecimal(100.00));

        //When
        accountService.transfer(random.nextLong(), transaction);
    }

    @Test(expected = NoCreditAvailableException.class)
    public void givenAccountWithNoCredit_whenTransfer_thenThrowNoCreditAvailableException() throws AccountNotFoundException {
        //Given
        Account accountFrom = accountService.saveAccount(new Account("50.00"));
        Account accountTo = accountService.saveAccount(new Account("0.50"));
        Transaction transaction = new Transaction(accountTo.getId(), new BigDecimal(100.00));

        //When
        accountService.transfer(accountFrom.getId(), transaction);
    }

    @Test(expected = InvalidAccountTransferException.class)
    public void givenDebitAndCreditSameAccount_whenTransfer_thenThrowInvalidAccountTransferException() {
        //Given
        Account account = accountService.saveAccount(new Account("50.00"));
        Transaction transaction = new Transaction(account.getId(), new BigDecimal(50));

        //When
        accountService.transfer(account.getId(), transaction);
    }

    @Test
    public void givenTwoConcurrentTransfers_whenTransfer_thenTransferSuccessfully() throws AccountNotFoundException, InterruptedException {
        //Given
        String amountFrom = "100.00";
        String amountTo = "0.00";
        Account accountFrom = accountService.saveAccount(new Account(amountFrom));
        Account accountTo = accountService.saveAccount(new Account(amountTo));
        Transaction transaction = new Transaction(accountTo.getId(), new BigDecimal(50.00));

        Runnable thread1 = () -> accountService.transfer(accountFrom.getId(), transaction);
        Runnable thread2 = () -> accountService.transfer(accountFrom.getId(), transaction);

        //When
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        Arrays.asList(thread1, thread2).forEach(executorService::submit);
        executorService.awaitTermination(100, TimeUnit.MILLISECONDS);

        assertEquals("Invalid amount transferred", amountTo, accountFrom.getBalance().toString());
        assertEquals("Invalid amount transferred", amountFrom, accountTo.getBalance().toString());
    }

    @Test
    public void givenTwoEqualsAccountsInSameOrder_whenTransfer_thenShouldNotDeadLock() throws InterruptedException {
        //Given
        String amountFrom = "100.00";
        String amountTo = "0.00";
        Account accountFrom = accountService.saveAccount(new Account(amountFrom));
        Account accountTo = accountService.saveAccount(new Account(amountTo));

        Transaction transaction = new Transaction(accountTo.getId(), new BigDecimal(10.00));
        Transaction opposedTxn = new Transaction(accountFrom.getId(), new BigDecimal(50.00));

        Runnable thread1 = () -> accountService.transfer(accountFrom.getId(), transaction);
        Runnable thread2 = () -> accountService.transfer(accountTo.getId(), opposedTxn);

        //When
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        Arrays.asList(thread1, thread2).forEach(executorService::submit);
        executorService.awaitTermination(100, TimeUnit.MILLISECONDS);

        assertEquals("Invalid amount transferred", "10.00", accountTo.getBalance().toString());
        assertEquals("Invalid amount transferred", "90.00", accountFrom.getBalance().toString());
    }

    @Test
    public void givenTwoConcurrentTransfers_whenTransfer_thenSubtractBothFromAccount() throws InterruptedException {
        //Given
        String amountFrom = "100.00";
        String amountTo = "0.00";
        Account accountFrom = accountService.saveAccount(new Account(amountFrom));
        Account accountTo = accountService.saveAccount(new Account(amountTo));
        Transaction transaction = new Transaction(accountTo.getId(), new BigDecimal(50.00));

        Runnable thread1 = () -> accountService.transfer(accountFrom.getId(), transaction);
        Runnable thread2 = () -> accountService.transfer(accountFrom.getId(), transaction);

        //When
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        Arrays.asList(thread1, thread2).forEach(executorService::submit);
        executorService.awaitTermination(100, TimeUnit.MILLISECONDS);

        assertEquals("Both values were not subtracted", amountTo, accountFrom.getBalance().toString());
    }

    @Test
    public void givenTwoConcurrentTransfers_whenTransfer_thenAddBothAmount() throws InterruptedException {
        //Given
        String amountFrom = "100.00";
        String amountTo = "0.00";
        Account accountFrom = accountService.saveAccount(new Account(amountFrom));
        Account accountTo = accountService.saveAccount(new Account(amountTo));

        Transaction transaction = new Transaction(accountTo.getId(), new BigDecimal(50.00));

        Runnable thread1 = () -> accountService.transfer(accountFrom.getId(), transaction);
        Runnable thread2 = () -> accountService.transfer(accountFrom.getId(), transaction);

        //When
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        Arrays.asList(thread1, thread2).forEach(executorService::submit);
        executorService.awaitTermination(100, TimeUnit.MILLISECONDS);

        assertEquals("Both values were not added", amountFrom, accountTo.getBalance().toString());
    }

    @Test
    public void givenAvailableAmount_whenTransfer_thenSubtractFromAccount() {
        //Given
        String amountFrom = "100.00";
        String amountTo = "0.00";
        Account accountFrom = accountService.saveAccount(new Account(amountFrom));
        Account accountTo = accountService.saveAccount(new Account(amountTo));
        Transaction transaction = new Transaction(accountTo.getId(), new BigDecimal(5));

        //When
        accountService.transfer(accountFrom.getId(), transaction);

        //Then
        assertEquals("Invalid amount transferred", "95.00", accountFrom.getBalance().toString());
    }

    @Test
    public void givenAvailableAmount_whenTransfer_thenAddToAccount() {
        //Given
        String amountFrom = "100.00";
        String amountTo = "0.00";

        Account accountFrom = accountService.saveAccount(new Account(amountFrom));
        Account accountTo = accountService.saveAccount(new Account(amountTo));
        Transaction transaction = new Transaction(accountTo.getId(), new BigDecimal(5));

        //When
        accountService.transfer(accountFrom.getId(), transaction);

        //Then
        assertEquals("Invalid amount transferred", "5.00", accountTo.getBalance().toString());
    }

    @Test(expected = AccountNotFoundException.class)
    public void givenAnInvalidAccountId_whenGetAccount_thenThrowAccountNotFoundException() {
        accountService.getAccount(1234L);
    }

    @Test(expected = AccountNotFoundException.class)
    public void givenAnInvalidAccountId_whenUpdateAccount_thenThrowAccountNotFoundException() {
        accountService.getAccount(1234L);
    }

    @Test
    public void givenAValidAccountId_whenUpdateAccount_thenReturnAccountUpdated() {
        //Given
        Account account = accountService.saveAccount(new Account("50.00"));
        account.setBalance(new BigDecimal(100.00));

        //When
        Account result = accountService.updateAccount(account.getId(), account);

        //Then
        assertEquals("The balance has not been updated", account.getBalance(), result.getBalance());
    }

    @Test
    public void givenAValidAccountId_whenGetAccount_sholdReturnAccount() {
        //Given
        Account account = new Account("50.00");
        accountService.saveAccount(account);

        //When
        Account result = accountService.getAccount(account.getId());

        //Then
        assertEquals(result.getId(), account.getId());
    }
}